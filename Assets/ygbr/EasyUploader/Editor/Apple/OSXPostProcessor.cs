﻿using UnityEditor;
using UnityEditor.Callbacks;

namespace ygbr.EasyUploader.Editor.Apple
{
    
    internal static class  AppleProcessor
    {
        private const int PRIORITY = int.MaxValue;

        /// <summary>
        /// Kicks off the build process for a given Apple target (macOS, iOS, tvOS).
        /// </summary>
        /// <param name="target">Platform we're building for (Apple-OSX, Steam-64).</param>
        /// <param name="pathToBuildProject">Location of the build path.</param>
        
        [PostProcessBuild(PRIORITY)]
        private static void ProcessBuild(BuildTarget target, string pathToBuildProject)
        {
            switch (target)
            {
                case BuildTarget.StandaloneOSX:
                    //TODO Link settings from window to processor
                    XcodeProcessor.Process("", pathToBuildProject);
                    XcodeCLI.ExportBuild("", pathToBuildProject);
                    XcodeCLI.DistributeBuild("", pathToBuildProject);
                    break;
                case BuildTarget.iOS:
                    //TODO XcodeProcess needs to support iOS variations
                    XcodeProcessor.Process("", pathToBuildProject);
                    XcodeCLI.ExportBuild("", pathToBuildProject);
                    XcodeCLI.DistributeBuild("", pathToBuildProject);
                    break;
                case BuildTarget.tvOS:
                    //TODO XcodeProcess needs to support tvOS variations
                    XcodeProcessor.Process("", pathToBuildProject);
                    XcodeCLI.ExportBuild("", pathToBuildProject);
                    XcodeCLI.DistributeBuild("", pathToBuildProject);
                    break;
            }
        }
    }
}