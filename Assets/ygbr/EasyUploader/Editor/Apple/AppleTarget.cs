﻿using System;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace ygbr.EasyUploader.Editor.Apple
{
    [Serializable]
    internal class AppleTarget : EasyUploaderTarget
    {
        public override string Name => "Apple";
        
        [SerializeField] private bool enablePostProcessing;
        [SerializeField] private bool enableDistribution;
        [SerializeField] private bool includeCloudKit;
        
        int inspectedSection = -1;
        AnimBool _showSubSection;
        
        
        private void OnEnable()
        {
            _showSubSection = new AnimBool(false);
        }
        

        //TODO Move platform-specific window drawing into its respective submodule
        public override Action DrawWindow()
        {
            Action inspectedElement = null;
            
            GUILayout.Toolbar(1, new[] {"iOS", "macOS", "tvOS"});

            GUILayout.BeginVertical(GUI.skin.box);
            
            //Store Credentials
            EditorGUILayout.TextField("Username", "AppleID Username");
            EditorGUILayout.PasswordField("Password", "Password");
            EditorGUILayout.Toggle("Show Password", false);
            if (GUILayout.Button("Save Credentials"))
            { }
            
            GUILayout.EndVertical();

            includeCloudKit = GUILayout.Toggle(includeCloudKit, "CloudKit");

            return inspectedElement;
        }


        //TODO Move platform-specific window drawing into its respective submodule
        private static void DrawXcodeCapabilities()
        {
            
            //TODO Make general UI.Header drawer
            GUILayout.BeginVertical();
            bool isEnabled = GUILayout.Toggle(false, "MODIFY CAPABILITIES", Styles.CENTERED_HEADER_TOGGLE);
            GUILayout.EndVertical();

            if (!isEnabled)
                return;
            
            if (GUILayout.Toggle(false, "Add iCloud"))
            {
                
            }
            
            if (GUILayout.Toggle(false, "Add GameCenter"))
            {
                
            }
            
            if (GUILayout.Toggle(false, "Add Push Notifications"))
            {
                
            }
        }

        private static void DrawXcodePlist()
        {
            GUILayout.BeginVertical();
            bool isEnabled = GUILayout.Toggle(false, "MODIFY INFO.PLIST", Styles.CENTERED_HEADER_TOGGLE);
            GUILayout.EndVertical();

            if (!isEnabled)
                return;
            
            if (GUILayout.Toggle(false, "Set Minimum OS Version"))
            {
                
            }
            
            if (GUILayout.Toggle(false, "Set Localizations"))
            {
                
            }
            
        }

        private static void DrawXcodeFrameworks()
        {
            GUILayout.BeginVertical();
            bool isEnabled = GUILayout.Toggle(false, "MODIFY FRAMEWORKS", Styles.CENTERED_HEADER_TOGGLE);
            GUILayout.EndVertical();

            if (!isEnabled)
                return;
            
            if (GUILayout.Toggle(false, "Include GameKit.Framework"))
            {
                
            }
            
            if (GUILayout.Toggle(false, "Include CloudKit.Framework"))
            {
                
            }
            
            if (GUILayout.Toggle(false, "Include GameController.Framework"))
            {
                
            }
        }

        private static void DrawXcodeEntitlements()
        {
            GUILayout.BeginVertical();
            bool isEnabled = GUILayout.Toggle(false, "MODIFY ENTITLEMENTS", Styles.CENTERED_HEADER_TOGGLE);
            GUILayout.EndVertical();

            if (!isEnabled)
                return;
            
            if (GUILayout.Toggle(false, "Include Sandbox Entitlements"))
            {
                
            }
            
            if (GUILayout.Toggle(false, "Include Hardened Runtime Entitlements"))
            {
                
            }
        }
        
        
    }
}