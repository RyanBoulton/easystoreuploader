﻿using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace ygbr.EasyUploader.Editor.Apple
{
    internal class OSXPreProcessor : IPreprocessBuildWithReport
    {

        public int callbackOrder => int.MaxValue;

        public void OnPreprocessBuild(BuildReport report)
        {
            Debug.Log("OSX PreProcessor Ran!");
        }
        
    }
}