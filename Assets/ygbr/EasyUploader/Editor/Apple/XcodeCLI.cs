﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using UnityEditor;
using Debug = UnityEngine.Debug;

namespace ygbr.EasyUploader.Editor.Apple
{
    /// <summary>
    /// Handles command line operations for xcode.
    /// Used for exporting builds from the project.
    /// Used for uploading exported builds to AppStore.
    /// </summary>
    public class XcodeCLI
    {
        private static string ProductName => PlayerSettings.productName;

        /// <summary>
        /// Method for archiving an Xcode project and priming for distribution to the AppStore
        /// </summary>
        /// <param name="properties"></param>
        /// <param name="pathToBuiltProject"></param>
        internal static void ExportBuild(string properties, string pathToBuiltProject)
        {
            string buildFolder = pathToBuiltProject.Replace($"/{ProductName}.xcodeproj", "");
            string exportOptions = $"{buildFolder}/ExportOptions.plist";
            string shellScript = $"{buildFolder}/ExportBuild.sh";

            //Write ExportOptions.plist File
            //TODO Write ExportOptions file
            Debug.Log($"Writing Export Options '{exportOptions}'");
            WriteFile(exportOptions, GenerateExportOptions(""));

            //Write ExportScript Script
            //TODO Write Export Script
            Debug.Log($"Writing Export Script '{shellScript}'");
            WriteFile(shellScript, GenerateExportScript(""));
            
            //Run Export Script
            Debug.Log($"Executing Export Script '{shellScript}'");
            RunScript(shellScript, OnExportBuildScriptExited);
        }

        /// <summary>
        /// Writes and executes a shell script to upload an exported build to AppStore
        /// XcodeCLI.ExportBuild should be called before this.
        /// </summary>
        /// <param name="properties"></param>
        /// <param name="pathToBuiltProject"></param>
        internal static void DistributeBuild(string properties, string pathToBuiltProject)
        {
            string buildFolder = pathToBuiltProject.Replace($"/{ProductName}.xcodeproj", "");
            string shellScript = $"{buildFolder}/Distribute.sh";
            
            //TODO Write Distribution Script
            Debug.Log($"Writing Distribute Script '{shellScript}'");
            WriteFile(shellScript, GenerateDistributionScript("", pathToBuiltProject));
            
            //Run Distribution Script
            Debug.Log($"Execute Distribute Script '{shellScript}'");
            RunScript(shellScript, OnDistributionScriptExit);
        }
        
        /// <summary>
        /// Execute a .sh file
        /// </summary>
        /// <param name="targetScript"></param>
        /// <param name="onScriptExit"></param>
        private static void RunScript(string targetScript, EventHandler onScriptExit)
        {
            //Fix permissions on script file
            var process = new Process();
            process.StartInfo = new ProcessStartInfo();
            process.StartInfo.FileName = "chmod";
            process.StartInfo.Arguments = $" +x \"{targetScript}\"";
            process.Start();
            process.WaitForExit();
            
            //Execute Script
            process.StartInfo = new ProcessStartInfo();
            process.StartInfo.FileName = "open";
            process.StartInfo.Arguments = $" -a terminal \"{targetScript}\"";
            process.StartInfo.CreateNoWindow = false;
            process.Start();
            process.Exited += onScriptExit;
        }

        
        #region CLI Callbacks

        private static void OnExportBuildScriptExited(object sender, EventArgs args)
        {
            Debug.Log($"OnExportBuildScriptExited!");
            Debug.Log($"'{sender}' :: {args}");
        }
        
        private static void OnDistributionScriptExit(object sender, EventArgs args)
        {
            Debug.Log($"OnDistributionScriptExited!");
            Debug.Log($"'{sender}' :: {args}");
        }
        
        #endregion
        
        #region Script & File Makers
        
        /// <summary>
        /// Creates ExportOptions.plist file from user set properties.
        /// </summary>
        /// <param name="properties"></param>
        /// <returns>.plist file contents</returns>
        private static string GenerateExportOptions(string properties)
        {
            //string exportOptionsFile = $"{buildFolder}/ExportOptions.plist";
            //string targetProjectPath = $"{pathToBuiltProject}/Unity-iPhone.xcodeproj";
            //string targetArchivePath = $"{buildFolder}/{ProductName}.xcarchive";
            //string targetPackagePath = $"{buildFolder}/{ProductName}.ipa/";
            return "";
        }

        /// <summary>
        /// Creates Export.sh file from user set properties.
        /// </summary>
        /// <param name="properties"></param>
        /// <returns>Export .sh script content</returns>
        private static string GenerateExportScript(string properties)
        {
            //string exportOptionsFile = $"{buildFolder}/ExportOptions.plist";
            //string targetProjectPath = $"{pathToBuiltProject}/Unity-iPhone.xcodeproj";
            //string targetArchivePath = $"{buildFolder}/{ProductName}.xcarchive";
            //string targetPackagePath = $"{buildFolder}/{ProductName}.ipa/";
            return "";
        }

        /// <summary>
        /// Creates Distribution.sh file from user set properties.
        /// </summary>
        /// <param name="properties"></param>
        /// <param name="pathToBuildProject"></param>
        /// <returns>.sh script content</returns>
        private static string GenerateDistributionScript(string properties, string pathToBuildProject)
        {
            BuildTarget buildTarget = BuildTarget.iOS;
            string targetArchivePath = "";
            string targetPackagePath = "";
            string exportOptionsFile = "";
            string accName = "";
            string accPass = "";
            string targetSdk= "";
            if (buildTarget == BuildTarget.StandaloneOSX) targetSdk = "macos";
            else if (buildTarget == BuildTarget.iOS) targetSdk = "iphoneos";
            else if (buildTarget == BuildTarget.tvOS) targetSdk = "appletvos";
            
            string script = "#!/bin/sh \n"
                + $"echo Preparing {buildTarget} for App Store \n"
                + "echo Archiving Project \n"
                + $"xcodebuild -project '{pathToBuildProject}' -scheme 'Unity-iPhone' -sdk {targetSdk} -configuration Release -archivePath '{targetArchivePath}' archive  >/dev/null \n"
                + "echo Exporting .ipa \n"
                + $"xcodebuild -exportArchive -archivePath '{targetArchivePath}' -exportOptionsPlist '{exportOptionsFile}' -exportPath '{targetPackagePath}'  >/dev/null \n"
                + "echo Uploading to App Store \n"
                + $"xcrun altool --upload-app -f '{targetPackagePath}/Unity-iPhone.ipa' -u {accName} -p {accPass}\n"
                + "echo Uploading Complete";
            
            return script;
        }

        #endregion

        #region IO Methods
        
        /// <summary>
        /// Use a FileStream to write contents to disk in UTF8
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="contents"></param>
        private static void WriteFile(string filePath, string contents)
        {
            using (FileStream fs = File.Create(filePath))
            {
                var info = new UTF8Encoding(true).GetBytes(contents);
                fs.Write(info, 0, info.Length);
            }
        }

        #endregion
    }
}