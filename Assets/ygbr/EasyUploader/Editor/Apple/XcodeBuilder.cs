﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditor.iOS.Xcode;
using UnityEngine;

namespace ygbr.EasyUploader.Editor.Apple
{
    
    internal static class XcodeProcessor
    {
        //TODO temporary forcing to use PlayerSettings.productName
        private static string ProductName => PlayerSettings.productName;

        internal static void Process(string properties, string pathToBuiltProject)
        {
            //-- Set Directories and Filenames --//
            //Step out of the generated .xcode project and into the app to locate the .plist and .entitlements files
            string applicationDir = pathToBuiltProject.Replace(".xcodeproj", "");
            string pListFileName = $"info.plist";
            string entitlementsFileName = $"{ProductName}.entitlements";
            //Full paths for target files
            string projectPath = $"{pathToBuiltProject}/project.pbxproj";
            string pListPath = $"{applicationDir}/{pListFileName}";
            string entitlementsPath = $"{applicationDir}/{entitlementsFileName}";

            //-- Set Build Properties --//
            var pbxProject = new PBXProject();
            pbxProject.ReadFromFile(projectPath);
            string targetGuid = pbxProject.TargetGuidByName(ProductName);
            //TODO swap out example properties
            pbxProject.SetBuildProperty(targetGuid, "OSX_DEPLOYMENT_TARGET", "13.0");
            pbxProject.SetBuildProperty(targetGuid, "IPHONEOS_DEPLOYMENT_TARGET", "13.0");
            pbxProject.SetBuildProperty(targetGuid, "TVOS_DEPLOYMENT_TARGET", "13.0");
            //Write Project Modifications to Disk
            pbxProject.WriteToFile(projectPath);
            
            //-- Add Frameworks  --//
            pbxProject.ReadFromFile(projectPath);
            targetGuid = pbxProject.TargetGuidByName(ProductName);
            //TODO swap out example frameworks
            Debug.Log($"Added {"CloudKit.framework"} Framework");
            pbxProject.AddFrameworkToProject(targetGuid, "CloudKit.framework", false);
            Debug.Log($"Added {"GameKit.framework"} Framework");
            pbxProject.AddFrameworkToProject(targetGuid, "GameKit.framework", false);
            Debug.Log($"Added {"GameController.framework"} Framework");
            pbxProject.AddFrameworkToProject(targetGuid, "GameController.framework", false);
            //Write Project Modifications to Disk
            pbxProject.WriteToFile(projectPath);

            //-- Add Capabilities --//
            //TODO Swap out example capabilities
            var capabilities = new ProjectCapabilityManager(projectPath, $"{ProductName}/{entitlementsFileName}", null, targetGuid);
            capabilities.AddiCloud(true, true, true, false, new string[] {} );
            capabilities.AddAppGroups(new []{ "com.Company.OtherGame" });
            capabilities.AddGameCenter();
            capabilities.AddPushNotifications(false);
            capabilities.AddKeychainSharing(new []{"com.Company.KeySharing"});
            //Write Project Modifications to Disk
            capabilities.WriteToFile();

            //-- Add Extra Entitlements --//
            //Load .entitlements file
            var entitlements = new PlistDocument();
            entitlements.ReadFromFile(entitlementsPath);
            //Update .entitlements file
            SetBoolKey(entitlements, "com.apple.security.app-sandbox", true);
            SetBoolKey(entitlements, "com.apple.security.device.bluetooth", true);
            SetBoolKey(entitlements, "com.apple.security.device.usb", true);
            SetBoolKey(entitlements, "com.apple.security.network.client", true);
            //Write Entitlements to disk
            entitlements.WriteToFile(entitlementsPath);
            
            //-- Set info.plist Properties  --//
            //Load info.plist file
            var pList = new PlistDocument();
            pList.ReadFromFile(pListPath);
            //Update .plist file
            SetStringKey(pList, "LSMinimumSystemVersion", "32.69");
            SetBoolKey(pList, "NSApplicationRequiresArcade", true);
            SetBoolKey(pList, "ITSAppUsesNonExemptEncryption", false);
            SetStringArray(pList, "CFBundleLocalizations", new [] { "en", "jp" });
            //Write .plist Modifications to disk
            pList.WriteToFile(pListPath);
        }
        
        #region PList Operations
        
        private static void SetBoolKey(PlistDocument doc, string key, bool value)
        {
            PlistElementDict elements = doc.root;
            elements.SetBoolean(key, value);
            Debug.Log($"info.plist set '{key}' to '{value}'");
        }

        private static void SetStringKey(PlistDocument doc, string key, string value)
        {
            PlistElementDict elements = doc.root;
            elements.SetString(key, value);
            Debug.Log($"info.plist set '{key}' to '{value}'");
        }

        private static void SetStringArray(PlistDocument doc, string key, IEnumerable<string> values)
        {
            PlistElementArray plistElementArray = doc.root.CreateArray(key);;
            foreach (string value in values)
            {
                plistElementArray.AddString(value);
                Debug.Log($"info.plist '{key}' item added: '{value}'");
            }
        }
        
        #endregion
        
    }
}