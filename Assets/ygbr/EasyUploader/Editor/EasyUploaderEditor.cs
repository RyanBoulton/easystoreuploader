﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace ygbr.EasyUploader.Editor
{
    public class EasyUploaderEditor : EditorWindow
    {
        private static EasyUploaderTarget[] _supportedPlatforms;
        private static int _focusedPlatform;

        private static Action _inspectedElement;

        [MenuItem("Window/EasyUploader/Settings")]
        private static void ShowWindow()
        {
            var window = GetWindow<EasyUploaderEditor>();
            window.titleContent = new GUIContent("EasyUploader");
            window.Show();
        }

        private void Awake()
        {
            var objects = new List<EasyUploaderTarget>();
            
            var types = Assembly.GetAssembly(typeof(EasyUploaderTarget)).GetTypes();
            foreach (Type type in types.Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(EasyUploaderTarget))))
            {
                objects.Add(EasyUploaderTarget.LoadAsset(type));
            }
            
            _supportedPlatforms = objects.ToArray();
        }
        

        private void OnGUI()
        {
            //Draw the main window options & info
            DrawMainHeader();
            
            GUILayout.BeginVertical(GUI.skin.box);
            Action inspectedElement = _supportedPlatforms[_focusedPlatform].DrawWindow();
            GUILayout.EndVertical();
            
            //Draw inspector if we have an available target
            DrawInspectedElement(inspectedElement);
            
            //Draw Bottom part of window that floats below the inspector
            DrawMainFooter();
        }

        private static void DrawMainHeader()
        {
            //Get Platform Name Labels
            string[] platformLabels = new string[_supportedPlatforms.Length];
            for (int i = 0; i < _supportedPlatforms.Length; i++) 
                platformLabels[i] = _supportedPlatforms[i].Name;

            //Draw Toolbar
            _focusedPlatform = GUILayout.Toolbar(_focusedPlatform, platformLabels);
        }
        
        private static void DrawInspectedElement(Action drawTarget)
        {
            if (drawTarget == null)
                return;

            GUILayout.BeginVertical(GUI.skin.box);
            drawTarget();
            GUILayout.EndVertical();
        }

        private static void DrawMainFooter()
        {
            
        }
    }

    internal static class Styles
    {
        internal static readonly GUIStyle CENTERED_HEADER_LABEL = new GUIStyle()
        {
            fontStyle = FontStyle.BoldAndItalic,
            alignment = TextAnchor.MiddleCenter,
            margin = new RectOffset(3, 3, 3, 3),
            normal = new GUIStyleState {textColor = EditorGUIUtility.isProSkin ? Color.white : Color.black}
        };

        internal static readonly GUIStyle CENTERED_HEADER_TOGGLE = new GUIStyle(GUI.skin.toggle)
        {
            border = new RectOffset(3, 3, 3, 3),
            fontStyle = FontStyle.BoldAndItalic,
            alignment = TextAnchor.MiddleLeft,
            margin = new RectOffset(3, 3, 3, 3),
            normal = new GUIStyleState {textColor = EditorGUIUtility.isProSkin ? Color.white : Color.black}
        };
    }
}