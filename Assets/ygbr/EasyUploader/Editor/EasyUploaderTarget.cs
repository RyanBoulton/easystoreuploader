﻿using System;
using UnityEditor;
using UnityEngine;

namespace ygbr.EasyUploader.Editor
{
    internal abstract class EasyUploaderTarget : ScriptableObject
    {
        public abstract string Name { get; }
        public abstract Action DrawWindow();

        private static EasyUploaderTarget CreateAsset(Type type)
        {
            //Create instance of platform asset and write to disk
            string filePath = $"Assets/{type.Name}.asset";
            var asset = CreateInstance(type) as EasyUploaderTarget;
            
            //Write, save & refresh project
            AssetDatabase.CreateAsset(asset, filePath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            
            //Return newly created asset
            return asset;
        }

        public static EasyUploaderTarget LoadAsset(Type type)
        {
            //Try finding asset guid
            var guids = AssetDatabase.FindAssets($"t:{type}");
            
            //Create new asset if missing
            if (guids.Length == 0)
                return CreateAsset(type);
            
            //Try loading asset from project
            string assetPath = AssetDatabase.GUIDToAssetPath(guids[0]);
            var asset = AssetDatabase.LoadAssetAtPath(assetPath, type) as EasyUploaderTarget;
            
            //Return loaded asset
            if (asset != null)
                return asset;
            
            //Default to creating a new asset for this platform
            return CreateAsset(type);
        }
        
    }
}