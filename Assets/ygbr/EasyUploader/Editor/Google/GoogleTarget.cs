﻿using System;
using UnityEditor;
using UnityEngine;

namespace ygbr.EasyUploader.Editor.Google
{
    internal class GoogleTarget : EasyUploaderTarget
    {
        public override string Name => "Google";
        
        [SerializeField] private string sdkPath;
        [SerializeField] private bool enablePostProcessing;
        [SerializeField] private bool enableDistribution;
        
        public override Action DrawWindow()
        {
            Action inspectedElement = null;

            GUILayout.BeginVertical(GUI.skin.box);
            
            EditorGUILayout.TextField("Username", "Username");
            EditorGUILayout.PasswordField("Password", "Password");
            
            GUILayout.EndVertical();

            return inspectedElement;
        }
    }
}