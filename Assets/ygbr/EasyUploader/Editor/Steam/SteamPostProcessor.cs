﻿using UnityEditor;
using UnityEditor.Callbacks;

namespace ygbr.EasyUploader.Editor.Steam
{
    internal static class SteamPostProcessor
    {
        private const int PRIORITY = int.MaxValue;
        
        [PostProcessBuild(PRIORITY)]
        private static void ProcessBuild(BuildTarget target, string pathToBuildProject)
        {
            switch (target)
            {
                case BuildTarget.StandaloneWindows64:
                case BuildTarget.StandaloneWindows:
                    break;
            }
        }
        
    }
    
}
