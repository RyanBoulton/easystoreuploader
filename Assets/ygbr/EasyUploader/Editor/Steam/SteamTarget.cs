﻿using System;
using UnityEditor;
using UnityEngine;

namespace ygbr.EasyUploader.Editor.Steam
{
    internal class SteamTarget : EasyUploaderTarget
    {
        public override string Name => "Steam";
        [SerializeField] private string steamSdkPath;
        [SerializeField] private bool enablePostProcessing;
        [SerializeField] private bool enableDistribution;
        
        public override Action DrawWindow()
        {
            Action inspectedElement = null;
            
            GUILayout.BeginVertical(GUI.skin.box);
            
            EditorGUILayout.TextField("Username", "Steam Username");
            EditorGUILayout.PasswordField("Password", "Password");
            
            GUILayout.EndVertical();

            return inspectedElement;
        }
    }
}