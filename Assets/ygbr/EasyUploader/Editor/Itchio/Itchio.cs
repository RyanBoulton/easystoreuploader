﻿using System;
using UnityEditor;
using UnityEngine;

namespace ygbr.EasyUploader.Editor.Itchio
{
    internal class Itchio : EasyUploaderTarget
    {
        public override string Name => "Itch.io";

        [SerializeField] private string butlerPath;
        [SerializeField] private bool enablePostProcessing;
        [SerializeField] private bool enableDistribution;

        
        public override Action DrawWindow()
        {
            Action inspectedElement = null;

            GUILayout.BeginVertical(GUI.skin.box);
            
            EditorGUILayout.TextField("Username", "Username");
            EditorGUILayout.PasswordField("Password", "Password");
            
            GUILayout.EndVertical();

            return inspectedElement;
        }
    }
}